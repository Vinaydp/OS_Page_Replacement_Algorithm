/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_paging_algo;

/**
 *
 * @author vinay
 */
public class SecondChanceLRUPageReplacer extends Pager { /*Using circular effect*/
    int circularPointer;
    int[] referenceBits;
    public SecondChanceLRUPageReplacer(int noOfFrames, String r) {
        super(noOfFrames, r);
        circularPointer = 0;
        referenceBits = new int[noOfFrames];
    }
    
    @Override
    public void allocateFrames() throws Exception {
        super.allocateFrames();
        String pageStr[];
        int index = 0;
        pageStr = refString.split(" ");
        int[]pageArray = new int[pageStr.length];
        for (int i = 0; i < pageArray.length; i++) {
            pageArray[i] = Integer.parseInt(pageStr[i]);
            if(pageArray[i] < 0)
                throw new Exception("Found a negative page number");
            
        }
        System.out.print(" Page Number    ");
        for(int i = 0 ; i < noOfFrames; i++) {
            System.out.printf("%5s | %-3s", "F"+(i+1), "Ref");
        }
        System.out.printf(" %10s %20s\n", "Hit/Miss", "Message");
        for(int page : pageArray) {        
            //search
            int sIndex;
            if((sIndex = searchIndex(page)) != -1 ) {
                //Hit
                referenceBits[sIndex]++;
                System.out.printf("%11s     ", page+"");
                printMemoryFrame();
                System.out.printf(" %8s\n", "HIT");
                hitCount++;
            }
            else {
                
                int frameNumber = insertAtFrame();
               /* if(frameNumber == -1) {
                    frameNumber = removeFrameAt(pageArray, index);
                    
                }*/
                insertAt(frameNumber, page);
                referenceBits[frameNumber] = 0;
                System.out.printf("%11s     ", page+"");
                printMemoryFrame();
                System.out.printf(" %8s %20s\n", "Miss", "Allocated at F"+(frameNumber + 1));
                
            }
            index++;
        }
        System.out.println("Hit Count : "+ hitCount);
        System.out.println("Page Faults : "+ (pageStr.length - hitCount));
    }
    
    int insertAtFrame() {
        
        while(referenceBits[circularPointer] != 0) {
            referenceBits[circularPointer]--;
            circularPointer = (circularPointer + 1) % noOfFrames;
        }
        int t = circularPointer;
        circularPointer = (circularPointer + 1) % noOfFrames;//point to next location
        return t;
    }
    
    @Override
    public void printMemoryFrame() {
        for(int i = 0; i < noOfFrames; i++) {
            if(circularPointer == i)
                System.out.print("->");
            else
                System.out.print("  ");
            System.out.printf("%3s | %-3s", (getPageAtFrame(i) == -1 ? "EMP" : getPageAtFrame(i)+""), referenceBits[i]+"");
        }
    }
}
