/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_paging_algo;

/**
 *
 * @author vinay
 */
abstract public class Pager {
    private int memoryFrames[];
    String refString;
    int noOfFrames;
    int hitCount;
    int possiblePosition;
    public Pager(int noOfFrames, String r) {
        memoryFrames = new int[noOfFrames];
        this.noOfFrames = noOfFrames;
        for(int i = 0; i < noOfFrames; i++) {
            memoryFrames[i] = -1;
        }
        refString = r;
    }
    
    public void insertAt(int i, int pageNumber) {
        memoryFrames[i] = pageNumber;
    }
    
    public int searchIndex(int pageNumber) {
        boolean got = false;
        for(int i = 0; i < noOfFrames; i++) {
            if(getPageAtFrame(i) == pageNumber) {
                return i;
            }
        }
        return -1;
    }
    
    public boolean search(int pageNumber) {
        boolean got = false;
        for(int i = 0; i < noOfFrames; i++) {
            if(memoryFrames[i] == pageNumber) {
                got = true;
            }
        }
        return got;
    }
    
    public int freeFrameAvailable() {
        for(int i = 0; i < noOfFrames; i++) {
            if(memoryFrames[i] == -1) {
                return i;
            }
        }
        return -1;
    }
    
    public int getPageAtFrame(int i) {
        return memoryFrames[i];
    }
    public void allocateFrames() throws Exception {
        if(refString == null || refString.length() == 0)
            throw new Exception("Reference String is Blank");
    }
    
    public void printMemoryFrame() {
        for(int i = 0; i < noOfFrames; i++) {
            System.out.printf("%5s", (memoryFrames[i] == -1 ? "EMP" : memoryFrames[i]+""));
        }
    }
}
