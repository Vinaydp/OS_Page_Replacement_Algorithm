/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_paging_algo;

/**
 *
 * @author vinay
 */
public class MostFrequentlyUsedPageReplacer extends LeastFrequentlyUsedPageReplacer{
    
    public MostFrequentlyUsedPageReplacer(int noOfFrames, String r) {
        super(noOfFrames, r);
    }
    @Override
    int removeFrameAt(int[] frequencyArray){
        int replaceframeNumber = 0;
        for(int i = 1; i < noOfFrames; i++) {
            if(frequencyArray[replaceframeNumber] < frequencyArray[i]) {
                replaceframeNumber = i;
            }
        }
        for(int i =0; i < fifoQueue.size(); i++) {
           if( frequencyArray[fifoQueue.get(i)] == frequencyArray[replaceframeNumber]) {
               replaceframeNumber = fifoQueue.remove(i);
               break;
           }
        }
        return replaceframeNumber;
    }
}
