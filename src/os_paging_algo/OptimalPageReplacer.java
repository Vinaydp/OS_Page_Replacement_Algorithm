/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_paging_algo;

/**
 *
 * @author vinay
 */
public class OptimalPageReplacer extends  Pager{
    public OptimalPageReplacer(int noOfFrames, String r) {
        super(noOfFrames, r);
    }
    
    @Override
    public void allocateFrames() throws Exception {
        super.allocateFrames();
        String pageStr[];
        int index = 0;
        pageStr = refString.split(" ");
        int[]pageArray = new int[pageStr.length];
        for (int i = 0; i < pageArray.length; i++) {
            pageArray[i] = Integer.parseInt(pageStr[i]);
            if(pageArray[i] < 0)
                throw new Exception("Found a negative page number");
            
        }
        System.out.print(" Page Number    ");
        for(int i = 0 ; i < noOfFrames; i++) {
            System.out.printf("%5s", "F"+(i+1));
        }
        System.out.printf(" %10s %20s\n", "Hit/Miss", "Message");
        for(int page : pageArray) {        
            //search
            if((search(page))) {
                //Hit
                System.out.printf("%11s     ", page+"");
                printMemoryFrame();
                System.out.printf(" %8s\n", "HIT");
                hitCount++;
            }
            else {
                
                int frameNumber = freeFrameAvailable();
                if(frameNumber == -1) {
                    frameNumber = removeFrameAt(pageArray, index);
                    
                }
                insertAt(frameNumber, page);
                System.out.printf("%11s     ", page+"");
                printMemoryFrame();
                System.out.printf(" %8s %20s\n", "Miss", "Allocated at F"+(frameNumber + 1));
                
            }
            index++;
        }
        System.out.println("Hit Count : "+ hitCount);
        System.out.println("Page Faults : "+ (pageStr.length - hitCount));
    }
    
    int removeFrameAt(int[] pageArray, int index){
        int replaceframeNumber = 0;
        int farLocation =  index;
        for(int i = 0; i < noOfFrames; i++) {
            boolean matched = false;
            for(int j = index + 1; j < pageArray.length; j++) {
                if(getPageAtFrame(i) == pageArray[j]) {
                    matched =true;
                    if(farLocation < j) {
                        farLocation = j;
                        replaceframeNumber = i;
                    }
                    break;
                }
            }
            if(!matched) {
                return i;
            }
        }
        return replaceframeNumber;
    }
}
