/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_paging_algo;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author vinay
 */
public class FCFSPageReplacer extends Pager{
    LinkedList<Integer> fifoQueue = new LinkedList<>();
    public FCFSPageReplacer(int noOfFrames, String r) {
        super(noOfFrames, r);
    }
    
    @Override
    public void allocateFrames() throws Exception {
        super.allocateFrames();
        String pageStr[];
        pageStr = refString.split(" ");
        System.out.print(" Page Number    ");
        for(int i = 0 ; i < noOfFrames; i++) {
            System.out.printf("%5s", "F"+(i+1));
        }
        System.out.printf(" %10s %20s\n", "Hit/Miss", "Message");
        for(String page : pageStr) {
            int pageNumber = Integer.parseInt(page);
            if(pageNumber < 0)
                throw new Exception("Found a negative page number");
            //search
            if((search(pageNumber))) {
                //Hit
                System.out.printf("%11s     ", page);
                printMemoryFrame();
                System.out.printf(" %8s\n", "HIT");
                hitCount++;
            }
            else {
                
                int frameNumber = freeFrameAvailable();
                if(frameNumber == -1) {
                    frameNumber = fifoQueue.removeFirst();
                    
                }
                insertAt(frameNumber, pageNumber);
                fifoQueue.add(frameNumber);
                System.out.printf("%11s     ", page);
                printMemoryFrame();
                System.out.printf(" %8s %20s\n", "Miss", "Allocated at F"+(frameNumber + 1));
                
            }
        }
        System.out.println("Hit Count : "+ hitCount);
        System.out.println("Page Faults : "+ (pageStr.length - hitCount));
    }
}
