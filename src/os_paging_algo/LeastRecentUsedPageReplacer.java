/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_paging_algo;

/**
 *
 * @author vinay
 */
public class LeastRecentUsedPageReplacer extends OptimalPageReplacer {
    
    public LeastRecentUsedPageReplacer(int noOfFrames, String r) {
        super(noOfFrames, r);
    }
    
    @Override
    int removeFrameAt(int[] pageArray, int index){
        int replaceframeNumber = 0;
        int farLocation =  index;
        for(int i = 0; i < noOfFrames; i++) {
            boolean matched = false;
            for(int j = index - 1; j >= 0 ; j--) {
                if(getPageAtFrame(i) == pageArray[j]) {
                    matched =true;
                    if(farLocation > j) {
                        farLocation = j;
                        replaceframeNumber = i;
                    }
                    break;
                }
            }
            if(!matched) {
                return i;
            }
        }
        return replaceframeNumber;
    }
}
