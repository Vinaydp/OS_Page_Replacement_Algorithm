/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_paging_algo;

import java.util.Scanner;

/**
 *
 * @author vinay
 */
public class OS_Paging_Algo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        Scanner inp = new Scanner (System.in);
        System.out.println("1) FIFO\n" +
                "\n" +
                "2) OPTIMAL\n" +
                "\n" +
                "3) LRU\n" +
                "\n" +
                "4) LFU\n" +
                "\n" +
                "5) MFU\n" +
                "\n" +
                "6) Second Chance\n" +
                "\n" +
                "7) Enhanced Second chance\n Enter your Choice");
        int choice  = inp.nextInt();
        Pager pager = null;
        System.out.println("Enter the no of frames in memory (to consider) : ");
        int noOfFrame = inp.nextInt();
        inp.nextLine();
        System.out.println("Enter the Reference String (with positive numbers only) . USE BLANK as DELIMITER : ");
        String r = inp.nextLine();
        System.out.println("Ref:"+r);
        switch(choice) {
            case 1:
                pager = new FCFSPageReplacer(noOfFrame, r);
                break;
            case 2:
                pager = new OptimalPageReplacer(noOfFrame, r);
                break;
            case 3:
                pager = new LeastRecentUsedPageReplacer(noOfFrame, r);
                break;
            case 4:
                pager = new LeastFrequentlyUsedPageReplacer(noOfFrame, r);
                break;
            case 5:
                pager = new MostFrequentlyUsedPageReplacer(noOfFrame, r);
                break;
            case 6:
                pager = new SecondChanceLRUPageReplacer(noOfFrame, r);
                break;
        }
        try {
            pager.allocateFrames();
        }
        catch(NullPointerException e){
            System.err.println("Wrong cchoice... so terminating the Program");
        }
        
    }
    
}
