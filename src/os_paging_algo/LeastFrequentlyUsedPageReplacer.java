/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os_paging_algo;

import java.util.LinkedList;

/**
 *
 * @author vinay
 */
public class LeastFrequentlyUsedPageReplacer extends Pager {
    int frequencyArray[];
    LinkedList<Integer> fifoQueue = new LinkedList<>();
    public LeastFrequentlyUsedPageReplacer(int noOfFrames, String r) {
        super(noOfFrames, r);
        frequencyArray = new int[noOfFrames];
    }
    
    
    
    
    @Override
    public void allocateFrames() throws Exception {
        super.allocateFrames();
        String pageStr[];
        int index = 0;
        pageStr = refString.split(" ");
        int[]pageArray = new int[pageStr.length];
        for (int i = 0; i < pageArray.length; i++) {
            pageArray[i] = Integer.parseInt(pageStr[i]);
            if(pageArray[i] < 0)
                throw new Exception("Found a negative page number");
            
        }
        System.out.print(" Page Number    ");
        for(int i = 0 ; i < noOfFrames; i++) {
            System.out.printf("%5s", "F"+(i+1));
        }
        System.out.printf(" %10s %20s\n", "Hit/Miss", "Message");
        for(int page : pageArray) {        
            //search
            int sIndex;
            if((sIndex = searchIndex(page)) != -1) {
                //Hit
                System.out.printf("%11s     ", page+"");
                printMemoryFrame();
                System.out.printf(" %8s\n", "HIT");
                frequencyArray[sIndex]++;
                hitCount++;
            }
            else {
                
                int frameNumber = freeFrameAvailable();
                if(frameNumber == -1) {
                    frameNumber = removeFrameAt(frequencyArray);
                    
                }
                insertAt(frameNumber, page);
                fifoQueue.add(frameNumber);
                frequencyArray[frameNumber] = 1;
                System.out.printf("%11s     ", page+"");
                printMemoryFrame();
                System.out.printf(" %8s %20s\n", "Miss", "Allocated at F"+(frameNumber + 1));
                
            }
            index++;
        }
        System.out.println("Hit Count : "+ hitCount);
        System.out.println("Page Faults : "+ (pageStr.length - hitCount));
    }
    
    int removeFrameAt(int[] frequencyArray){
        int replaceframeNumber = 0;
        for(int i = 1; i < noOfFrames; i++) {
            if(frequencyArray[replaceframeNumber] > frequencyArray[i]) {
                replaceframeNumber = i;
            }
        }
        
        for(int i =0; i < fifoQueue.size(); i++) {
           if( frequencyArray[fifoQueue.get(i)] == frequencyArray[replaceframeNumber]) {
               replaceframeNumber = fifoQueue.remove(i);
               break;
           }
        }
        return replaceframeNumber;
    }
}
